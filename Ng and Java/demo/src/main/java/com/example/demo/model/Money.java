package com.example.demo.model;

import lombok.Data;

@Data
public class Money {
    private String code;
    private String name;
    //private  String nominal;
    private double value;

}
