package com.example.demo.controller;

import com.example.demo.model.Money;
import com.example.demo.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController


public class MoneyController {

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/")
    public List<Money> getZipFile() {

        MoneyService service = new MoneyService();
        return     service.parser();

    }

}
