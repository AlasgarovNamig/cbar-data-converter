package com.example.demo.service;

import com.example.demo.model.Money;
import org.json.*;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MoneyService {

    public static String xml = getEmployees();
//    DecimalFormat df = new DecimalFormat("#.######");

    public List<Money> parser() {
        List<Money> moneyList = new ArrayList<>();

        try {
            JSONObject json = XML.toJSONObject(xml);


            for (int i = 0; i < json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").length(); i++) {
                String code=new String(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getString("Code").getBytes("ISO-8859-1"), "UTF-8");
                String name = new String(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getString("Name").getBytes("ISO-8859-1"), "UTF-8");
                String nominal =new String(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getString("Nominal").getBytes("ISO-8859-1"), "UTF-8");
                double value = json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getDouble("Value");


                Money money = new Money();
                money.setName(modifyName(name));
                money.setCode(code);
                money.setValue(value/Integer.parseInt(nominal));

//                money.setName(new String(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getString("Name").getBytes("ISO-8859-1"), "UTF-8"));
//                money.setValue(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getDouble("Value"));
//                money.setNominal(new String(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getString("Name").getBytes("ISO-8859-1"), "UTF-8"));
//                money.setCode(new String(json.getJSONObject("ValCurs").getJSONArray("ValType").getJSONObject(1).getJSONArray("Valute").getJSONObject(i).getString("Code").getBytes("ISO-8859-1"), "UTF-8"));
                moneyList.add(money);
            }


//            String encodedWithISO88591 = "dollarÄ±";
//            String decodedToUTF8 = new String(encodedWithISO88591.getBytes("ISO-8859-1"), "UTF-8");


            return moneyList;
        } catch (JSONException | UnsupportedEncodingException e) {

        }
        return null;
    }


    private static String getEmployees() {
        final String uri = "https://www.cbar.az/currencies/27.05.2021.xml";

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);


        return result;

    }

    private  String modifyName(String name){
        String x ="";
        String[] arr = name.split(" ", 0);
        for(int i =1;i<arr.length;i++){

            x+= arr[i]+" ";

        }
        return x;
    }

}
